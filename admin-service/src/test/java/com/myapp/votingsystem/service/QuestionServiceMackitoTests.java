package com.myapp.votingsystem.service;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import com.myapp.votingsystem.Exception.EmptyInputException;
import com.myapp.votingsystem.model.Options;
import com.myapp.votingsystem.model.Question;
import com.myapp.votingsystem.repositry.OptionRepositry;
import com.myapp.votingsystem.repositry.QuestionRepositry;

@TestMethodOrder(OrderAnnotation.class)
@ComponentScan(basePackages ="com.myapp.votingsystem")
@SpringBootTest(classes= {QuestionServiceMackitoTests.class})
public class QuestionServiceMackitoTests {

	@Mock
	private QuestionRepositry questionRepositry;
	
	@Mock
	private OptionRepositry optionRepositry;
	
	@InjectMocks
	public  QuestionService questionService;
	
	@Test
	@Order(1)
	public void test_creatingPoll(){
		
		List<Options> option=new ArrayList<Options>(Arrays.asList(new Options(1,"YES"),new Options(2,"NO")));
		Question question=new Question();
		question.setId(1);
		question.setOptions(option);
		question.setQuestionName("Is Lockdown Neccessory");
		when(questionRepositry.save(question)).thenReturn(question);
		questionService.createPoll(question);
		assertEquals(1,questionService.createPoll(question).getId());
		assertEquals("Is Lockdown Neccessory",questionService.createPoll(question).getQuestionName());
		assertEquals(option,questionService.createPoll(question).getOptions());
		assertEquals(question,questionService.createPoll(question));
		
		Question question1=new Question();
		when(questionRepositry.save(question1)).thenReturn(null);
		assertEquals(null,questionService.createPoll(question1));
	}
	
	@Test
	@Order(2)
	public void test_getPollById() {
	
		List<Options> option1=new ArrayList<Options>(Arrays.asList(new Options(1,"YES"),new Options(2,"NO")));
		List<Options> option2=new ArrayList<Options>(Arrays.asList(new Options(1,"Few days Lockdown"),new Options(2,"Night Curfew")));
		List<Question> questions=new ArrayList<>(Arrays.asList(new Question(1,"Is Lockdown Neccessory",option1),
												new Question(2,"which is betterfor the present situation",option2)));
		int questionId=1;
		when(questionRepositry.findAll()).thenReturn(questions);
		questionService.getPollById(questionId);
		assertEquals(questionId,questionService.getPollById(questionId).getId());
	}
	
	@Test
	@Order(3)
	public void test_getExistingPolls() {
		
		List<Options> option1=new ArrayList<Options>(Arrays.asList(new Options(1,"YES"),new Options(2,"NO")));
		List<Options> option2=new ArrayList<Options>(Arrays.asList(new Options(1,"Few days Lockdown"),new Options(2,"Night Curfew")));
		List<Question> questions=new ArrayList<>(Arrays.asList(new Question(1,"Is Lockdown Neccessory",option1),
												new Question(2,"which is betterfor the present situation",option2)));
		
		when(questionRepositry.findAll()).thenReturn(questions);
		questionService.getExistingPolls();
		assertEquals(questions,questionService.getExistingPolls());
	}
	
	
	@Test
	@Order(5)
	public void deletePollByIdTest() {
		
		
		int id=1;
		
		assertEquals("Deleted",questionService.deleteById(id));
		verify(questionRepositry,times(1)).deleteById(id);
	}
}
