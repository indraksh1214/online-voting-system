package com.myapp.votingsystem.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import com.myapp.votingsystem.model.Votes;
import com.myapp.votingsystem.repositry.VotesRepository;


@TestMethodOrder(OrderAnnotation.class)
@ComponentScan("com.myapp.votingsystem")
@SpringBootTest(classes = {VotesServiceMackitoTest.class})
public class VotesServiceMackitoTest {
	
	@Mock
	private VotesRepository votesRepository;
	
	@InjectMocks
	private VotesService votesService;
	
	@Test
	@Order(1)
	public void test_saveVotes() {
		
		Votes votes=new Votes();
		votes.setOptionId(1);
		votes.setQuestionId(1);
		votes.setVotes((long)10);
		when(votesRepository.save(votes)).thenReturn(votes);
		assertEquals(1,votesService.saveVotes(votes).getQuestionId());
		assertEquals(1,votesService.saveVotes(votes).getOptionId());
		assertEquals((long)10,votesService.saveVotes(votes).getVotes());
		assertEquals(votes, votesService.saveVotes(votes));
		
	}
	
	@Test
	@Order(2)
	public void test_findByQuestionId() {
		
		Votes votes=new Votes(1,1,(long) 10);
		Votes votes1=new Votes(1,2,(long) 9);
		Votes votes2=new Votes(1,3,(long) 8);
		Votes votes3=new Votes(1,4,(long) 7);
		
		List<Votes> allVotes= new ArrayList<>();
		allVotes.add(votes);
		allVotes.add(votes1);
		allVotes.add(votes2);
		allVotes.add(votes3);
		
		int questionId=1;
		when(votesRepository.findByQuestionId(questionId)).thenReturn(allVotes);
		
		votesService.findByQuestionId(questionId);
		assertEquals(226, votesService.findByQuestionId(questionId).length());
	}
}
