package com.myapp.votingsystem.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import com.myapp.votingsystem.model.Options;
import com.myapp.votingsystem.repositry.OptionRepositry;

@ComponentScan(basePackages ="com.myapp.votingsystem")
@SpringBootTest(classes= {OptionServiceMackitoTest.class})
public class OptionServiceMackitoTest {

	@Mock
	private OptionRepositry optionRepositry;
	
	
	@InjectMocks
	public OptionService optionService;
	
	Options option1=new Options(1,"YES");
	Options option2=new Options(2,"NO");
	List<Options> options;
	
	
	@Test
	@Order(2)
	public void test_findById() {
		
		List<Options> options=new ArrayList<>();
		options.add(option1);
		options.add(option2);
		int optionId=1;
		when(optionRepositry.findAll()).thenReturn(options);
			
		assertEquals(option1,optionService.findById(optionId));  
	}
	
	@Test
	@Order(1)
	public void test_getAllOptions() {
		
		List<Options> options=new ArrayList<>();
		options.add(option1);
		options.add(option2);
		when(optionRepositry.findAll()).thenReturn(options);
		
		assertEquals(2,optionService.getAllOptions().size());
	}

	@Test
	@Order(3)
	public void test_deleteById() {
		
		int optionId=1;
	
		assertEquals("Deleted",optionService.deleteById(optionId));
		verify(optionRepositry,
				times(1)).deleteById(optionId); 
	}
	
	@Test
	@Order(4)
	public void test_save() {
		
		Options option=new Options();
		option.setId(1);
		option.setOptionName("Nothing");
		when(optionRepositry.save(option)).thenReturn(option);
		
		assertEquals(1,optionRepositry.save(option).getId());
		assertEquals("Nothing",optionRepositry.save(option).getOptionName());
		assertEquals(option,optionService.save(option));
		
		
		when(optionRepositry.save(option)).thenReturn(null);
		assertEquals(null,optionService.save(option));
	}
	
}
