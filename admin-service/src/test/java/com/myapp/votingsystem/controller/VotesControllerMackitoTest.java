package com.myapp.votingsystem.controller;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import com.myapp.votingsystem.Exception.NotFoundException;
import com.myapp.votingsystem.model.Votes;
import com.myapp.votingsystem.proxy.VoterServiceProxy;
import com.myapp.votingsystem.service.VotesService;

@TestMethodOrder(OrderAnnotation.class)
@ComponentScan(basePackages = "com.myapp.votingsystem")
@SpringBootTest(classes = { VotesControllerMackitoTest.class })
public class VotesControllerMackitoTest {

	@Mock
	private VotesService votesService;
	
	@Mock
	private VoterServiceProxy voterServiceProxy;
	
	@InjectMocks
	private VotesController votesController;
	
	@Test
	@Order(1)
	public void test_calculatingVotesForEachOption() {
		
		Long votes=(long) 10;
		Votes votesForOption=new Votes();
		votesForOption.setOptionId(1);
		votesForOption.setQuestionId(1);
		votesForOption.setVotes(votes);
		
		int questionId=1;
		int optionId=1;
		when(voterServiceProxy.calculatingVotes(questionId, optionId)).thenReturn(votes);
		when(votesService.saveVotes(votesForOption)).thenReturn(votesForOption);
		
		assertEquals("saved",votesController.calculatingVotesForEachOption(questionId, optionId));
		
		Long vote1=(long)-1;
		when(voterServiceProxy.calculatingVotes(questionId, optionId)).thenReturn(vote1);
		when(votesService.saveVotes(votesForOption)).thenReturn(votesForOption);
		
		assertEquals("the combination not available",votesController.calculatingVotesForEachOption(questionId, optionId));
	
		when(voterServiceProxy.calculatingVotes(questionId, optionId)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{votesController.calculatingVotesForEachOption(questionId, optionId);});
	}
	
	@Test
	@Order(2)
	public void test_FetchingDetailsToGetResult() {
		
		Votes votes=new Votes(1,1,(long) 10);
		Votes votes1=new Votes(1,2,(long) 9);
		Votes votes2=new Votes(1,3,(long) 8);
		Votes votes3=new Votes(1,4,(long) 7);
		
		List<Votes> allVotes= new ArrayList<>();
		allVotes.add(votes);
		allVotes.add(votes1);
		allVotes.add(votes2);
		allVotes.add(votes3);
		
		int id=1;
		String result=allVotes+"\n"+"THE WINNER OpionId IS-->"+votes.getOptionId()+" WITH THE VOTES--->"+votes.getVotes();
		
		when(votesService.findByQuestionId(id)).thenReturn(result);
		
		assertEquals(224,votesController.FetchingDetailsToGetResult(id).length());
	}
	
	@Test
	@Order(3)
	public void test_conductingPolling() {
		
		int questionId=1;
		int optionId=1;
		int voterId=1;
		String comment="hello";
		String message="polled";
		when(voterServiceProxy.conductingPolling(questionId, optionId, voterId, comment)).thenReturn(message);
		
		assertEquals(message,votesController.conductingPolling(questionId, optionId, voterId, comment));
	}
}
