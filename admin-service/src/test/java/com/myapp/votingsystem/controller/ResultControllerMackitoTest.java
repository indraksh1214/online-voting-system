package com.myapp.votingsystem.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import com.myapp.votingsystem.Exception.NotFoundException;
import com.myapp.votingsystem.model.Votes;
import com.myapp.votingsystem.proxy.VoterServiceProxy;

@TestMethodOrder(OrderAnnotation.class)
@ComponentScan(basePackages = "com.myapp.votingsystem")
@SpringBootTest(classes = { ResultControllerMackitoTest.class })
public class ResultControllerMackitoTest {
	

	@Mock
	private VotesController votesController;
	
	@Mock
	private VoterServiceProxy voterServiceProxy;
	
	@InjectMocks
	private ResultController resultController;
	
	
	@Test
	@Order(1)
	public void test_finalResult() {
		
		Votes votes=new Votes(1,1,(long) 10);
		Votes votes1=new Votes(1,2,(long) 9);
		Votes votes2=new Votes(1,3,(long) 8);
		Votes votes3=new Votes(1,4,(long) 7);
		
		List<Votes> allVotes= new ArrayList<>();
		allVotes.add(votes);
		allVotes.add(votes1);
		allVotes.add(votes2);
		allVotes.add(votes3);
	
		int questionId=1;
		String result=allVotes+"\n"+"THE WINNER OpionId IS-->"+votes.getOptionId()+" WITH THE VOTES--->"+votes.getVotes();
		
		when(votesController.FetchingDetailsToGetResult(questionId)).thenReturn(result);
		
		assertEquals(224,resultController.finalResult(questionId).length());
		
		when(votesController.FetchingDetailsToGetResult(questionId)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{resultController.finalResult(questionId);});
	}
	
	@Test
	@Order(1)
	public void test_fetchComments() {
		
		String[] comments= {"qwer","asd","fgh"};
		int questionId=1;
		int optionId=1;
		when(voterServiceProxy.getComments(questionId,optionId)).thenReturn(comments);
		assertEquals(3,resultController.fetchComments(questionId,optionId).length);
	
		when(voterServiceProxy.getComments(questionId,optionId)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{resultController.fetchComments(questionId,optionId);});
	}
}
