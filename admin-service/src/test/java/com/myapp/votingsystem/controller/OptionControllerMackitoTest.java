package com.myapp.votingsystem.controller;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import com.myapp.votingsystem.Exception.NotFoundException;
import com.myapp.votingsystem.model.Options;
import com.myapp.votingsystem.service.OptionService;

@TestMethodOrder(OrderAnnotation.class)
@ComponentScan(basePackages = "com.myapp.votingsystem")
@SpringBootTest(classes = { OptionControllerMackitoTest.class })
public class OptionControllerMackitoTest {

	@Mock
	private OptionService optionService;

	@InjectMocks
	private OptionController optionController;

	@Test
	@Order(1)
	public void test_updateOption() {

		Options option = new Options(1, "YES");

		when(optionService.findById(option.getId())).thenReturn(option);
		when(optionService.save(option)).thenReturn(option);
		assertEquals(1, optionController.updateOption(option).getId());
		assertEquals("YES", optionController.updateOption(option).getOptionName());
		assertEquals(option, optionController.updateOption(option));
		
		when(optionService.findById(option.getId())).thenReturn(null);
		assertEquals(null, optionController.updateOption(option));
		
		when(optionService.findById(option.getId())).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{optionController.updateOption(option);});
	}

	@Test
	@Order(2)
	public void test_getOption() {

		Options option = new Options(1, "YES");

		int id = 1;

		when(optionService.findById(id)).thenReturn(option);

		assertEquals(option, optionController.getOption(id));
		
		when(optionService.findById(id)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{optionController.getOption(id);});
	}

	@Test
	@Order(2)
	public void test_getAllOptions() {

		Options option1 = new Options(1, "YES");
		Options option2 = new Options(2, "NO");
		List<Options> options = new ArrayList<>();
		options.add(option1);
		options.add(option2);

		when(optionService.getAllOptions()).thenReturn(options);
		assertEquals(2, optionController.getAllOptions().size());

		when(optionService.getAllOptions()).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{optionController.getAllOptions();});
	}

	@Test
	@Order(2)
	public void test_daleteOptions() {
		
		Options option1=new Options(1,"YES");
		int id=1;
		String message="Deleted";
		when(optionService.deleteById(id)).thenReturn(message);
		assertEquals("Deleted",optionController.daleteOptions(id));
		
		when(optionService.deleteById(id)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{optionController.daleteOptions(id);});
	}
}
