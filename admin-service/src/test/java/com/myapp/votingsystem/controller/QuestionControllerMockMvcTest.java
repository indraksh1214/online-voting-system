package com.myapp.votingsystem.controller;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import com.myapp.votingsystem.Exception.NotFoundException;
import com.myapp.votingsystem.model.Options;
import com.myapp.votingsystem.model.Question;
import com.myapp.votingsystem.service.OptionService;
import com.myapp.votingsystem.service.QuestionService;

@TestMethodOrder(OrderAnnotation.class)
@ComponentScan(basePackages = "com.myapp.votingsystem")
@SpringBootTest(classes = { QuestionControllerMockMvcTest.class })
public class QuestionControllerMockMvcTest {

	
	@Mock
	QuestionService questionService;
	
	@Mock
	OptionService optionService;

	@InjectMocks
	QuestionController questionController;

	List<Options> options;
	Options option;

	List<Question> questions;
	Question question;

//	
//	List<Options> option1=new ArrayList<Options>(Arrays.asList(new Options(1,"YES"),new Options(2,"NO")));
//	List<Options> option2=new ArrayList<Options>(Arrays.asList(new Options(1,"Few days Lockdown"),new Options(2,"Night Curfew")));
//	List<Question> questions=new ArrayList<>(Arrays.asList(new Question(1,"Is Lockdown Neccessory",option1),
//											new Question(2,"which is betterfor the present situation",option2)));
//	

	@Test
	@Order(1)
	public void test_createPoll()  {

		List<Options> option = new ArrayList<Options>(Arrays.asList(new Options(1, "YES"), new Options(2, "NO")));
		question = new Question(1, "Is Lockdown Neccessory", option);

		when(questionService.createPoll(question)).thenReturn(question);
		
		assertEquals("poll Added Successfully",questionController.creatingPoll(question));
	}

	@Test
	@Order(2)
	public void test_getPollById()  {

		List<Options> option = new ArrayList<Options>(Arrays.asList(new Options(1, "YES"), new Options(2, "NO")));
		question = new Question(1, "Is Lockdown Neccessory", option);

		int questionId = 1;
		when(questionService.getPollById(questionId)).thenReturn(question);

		assertEquals(question,questionController.getPollById(questionId));
		
		int questionId1 = 2;
		when(questionService.getPollById(questionId1)).thenReturn(null);

		assertEquals(null,questionController.getPollById(questionId1));
	}

	@Test
	@Order(3)
	public void test_getExistingPolls() throws Exception {

		List<Options> option1 = new ArrayList<Options>(Arrays.asList(new Options(1, "YES"), new Options(2, "NO")));
		List<Options> option2 = new ArrayList<Options>(
				Arrays.asList(new Options(1, "Few days Lockdown"), new Options(2, "Night Curfew")));
		questions = new ArrayList<>(Arrays.asList(new Question(1, "Is Lockdown Neccessory", option1),
				new Question(2, "which is better in present situation", option2)));

		when(questionService.getExistingPolls()).thenReturn(questions);
		assertEquals(questions,questionController.getExistingPolls());
		
		when(questionService.getExistingPolls()).thenReturn(null);
		assertEquals(null,questionController.getExistingPolls());

	}
	
	@Test
	@Order(4)
	public void test_isOptionExist() throws Exception {
		
		Options option1= new Options(1, "YES");
		Options option2=new Options(2, "NO");
		List<Options> options=new ArrayList<Options>();
		options.add(option1);
		options.add(option2);
		
		question = new Question(1, "Is Lockdown Neccessory", options);
		
		int questionId=1;
		int optionId=1;
		when(optionService.findById(optionId)).thenReturn(option1);
		when(questionService.getPollById(questionId)).thenReturn(question);
		
		
		assertEquals(true,questionController.isOptionExist(questionId, optionId));
		
		when(optionService.findById(optionId)).thenReturn(null);
		when(questionService.getPollById(questionId)).thenReturn(question);
		
		
		assertEquals(false,questionController.isOptionExist(questionId, optionId));
		
		when(optionService.findById(optionId)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{questionController.isOptionExist(questionId, optionId);});
		
		when(questionService.getPollById(questionId)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{questionController.isOptionExist(questionId, optionId);});
		
	}	
	
	@Test
	@Order(5)
	public void test_deletePollById() throws Exception{
		
		List<Options> option = new ArrayList<Options>(Arrays.asList(new Options(1, "YES"), new Options(2, "NO")));
		question = new Question(1, "Is Lockdown Neccessory", option);
		
		int pollId=1;
		
		assertEquals("Deleted",questionController.deletePollById(pollId));
		verify(questionService,times(1)).deleteById(pollId);
	
		when(questionService.deleteById(pollId)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{questionController.deletePollById(pollId);});
	}

}
