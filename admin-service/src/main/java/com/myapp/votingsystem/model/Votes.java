package com.myapp.votingsystem.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Votes {
	
	private int questionId;
	@Id
	private int optionId;
	private Long votes;
	
	public Votes() {

	}
	
	public Votes(int questionId, int optionId, Long votes) {
		super();
		this.questionId = questionId;
		this.optionId = optionId;
		this.votes = votes;
	}
	
	
	
	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
	public int getOptionId() {
		return optionId;
	}
	public void setOptionId(int optionId) {
		this.optionId = optionId;
	}
	public Long getVotes() {
		return votes;
	}
	public void setVotes(Long votes) {
		this.votes = votes;
	}

	@Override
	public String toString() {
		return "Votes [questionId=" + questionId + ", optionId=" + optionId + ", votes=" + votes + "]"+"\n";
	}
	
}
