package com.myapp.votingsystem.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="Question")
public class Question {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@NotNull(message = "name should not null")
	@Size(min=2,message="Name should contain atleast two characters")
	private String questionName;
	
	@NotEmpty
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="questio_id",referencedColumnName="id")
	private List<Options> options;

	public Question(int id, String questionName, List<Options> options) {
		super();
		this.id = id;
		this.questionName = questionName;
		this.options = options;
	}
	
	public Question() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public List<Options> getOptions() {
		return options;
	}

	public void setOptions(List<Options> options) {
		this.options = options;
	}
	
	
}
