package com.myapp.votingsystem.Exception;

public class EmptyInputException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3094385135940755071L;
	private String errorCode;
	private String errorMassagel;
	public EmptyInputException(String errorCode, String errorMassagel) {
		super();
		this.errorCode = errorCode;
		this.errorMassagel = errorMassagel;
	}
	public EmptyInputException() {
		
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMassagel() {
		return errorMassagel;
	}
	public void setErrorMassagel(String errorMassagel) {
		this.errorMassagel = errorMassagel;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
