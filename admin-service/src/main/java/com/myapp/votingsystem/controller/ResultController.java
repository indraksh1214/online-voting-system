package com.myapp.votingsystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.myapp.votingsystem.Exception.NotFoundException;
import com.myapp.votingsystem.proxy.VoterServiceProxy;

@RestController
public class ResultController {
	
	@Autowired
	private VotesController votesController;
	
	@Autowired
	private VoterServiceProxy voterServiceProxy;
	
	@GetMapping("/{questionId}/finalResult")
	public String finalResult(@PathVariable("questionId")int questionId)
	{
		try {
		return votesController.FetchingDetailsToGetResult(questionId);
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}
	
	@GetMapping("/{questionId}/{optionId}/getComments")
	public String[] fetchComments(@PathVariable("questionId")int questionId
							,@PathVariable("optionId")int optionId)
	{
		try {
		return voterServiceProxy.getComments(questionId,optionId);
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}

}
