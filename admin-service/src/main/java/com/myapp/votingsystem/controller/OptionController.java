package com.myapp.votingsystem.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.myapp.votingsystem.Exception.NotFoundException;
import com.myapp.votingsystem.model.Options;
import com.myapp.votingsystem.service.OptionService;

@RestController
public class OptionController {
	
	@Autowired
	public OptionService optionService;
	
	@PutMapping("/updateOption")
	public Options updateOption(@RequestBody Options option)
	{
		try {
		
		Options option1=optionService.findById(option.getId());
		if(option1!=null) {
			return optionService.save(option1);
		}else {
			return null;
		}
		}catch(Exception e) {
			throw new NotFoundException();
		}		
	}
	@GetMapping("/{id}/getOption")
	public Options getOption(@PathVariable("id")int id)
	{
		try {
		return optionService.findById(id);
		}catch(Exception e) {
			throw new NotFoundException();
		}	
	}
	
	@GetMapping("/getAllOptions")
	public List<Options> getAllOptions()
	{
		try {
		return optionService.getAllOptions();
		}catch(Exception e) {
			throw new NotFoundException();
		}	
	}
	
	@DeleteMapping("/{id}/delteOption")
	public String daleteOptions(@PathVariable("id")int id)
	{
		try {
		return optionService.deleteById(id);
		}catch(Exception e) {
			throw new NotFoundException();
		}	
	}
}
