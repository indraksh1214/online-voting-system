package com.myapp.votingsystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myapp.votingsystem.Exception.NotFoundException;
import com.myapp.votingsystem.model.Votes;
import com.myapp.votingsystem.proxy.VoterServiceProxy;
import com.myapp.votingsystem.service.VotesService;

@RestController
public class VotesController {

	@Autowired
	private VoterServiceProxy voterServiceProxy;

	@Autowired
	private VotesService votesService;

	@PostMapping("/{questionId}/{optionId}/calculatingVotesForEachOption")
	public String calculatingVotesForEachOption(@PathVariable("questionId") int questionId,
							@PathVariable("optionId") int optionId) {
		
		
		try {		
		Long vote = voterServiceProxy.calculatingVotes(questionId, optionId);
		if(vote<=-1) {
			return "the combination not available";
		}else {
			Votes save=new Votes(questionId,optionId,vote);
		votesService.saveVotes(save);
		return "saved";
		}
		}catch(Exception e) {
			throw new NotFoundException();
		}
		
	}

	@PostMapping("/{questionId}/{optionId}/{voterId}/{comment}/polling")
	public String conductingPolling(@PathVariable("questionId") int questionId, @PathVariable("optionId") int optionId,
			@PathVariable("voterId") int voterId, @PathVariable("comment") String comment) {
		
		return voterServiceProxy.conductingPolling(questionId, optionId, voterId, comment);
	}
	
	@GetMapping("/{questionId}")
	public String FetchingDetailsToGetResult(@PathVariable("questionId") int id) {

		return votesService.findByQuestionId(id);
	}
}
