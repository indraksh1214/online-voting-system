package com.myapp.votingsystem.controller;

import java.util.List;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.myapp.votingsystem.Exception.NotFoundException;
import com.myapp.votingsystem.model.Options;
import com.myapp.votingsystem.model.Question;
import com.myapp.votingsystem.service.OptionService;
import com.myapp.votingsystem.service.QuestionService;

@RestController

public class QuestionController {
	
	@Autowired
	public  QuestionService questionService;
	
	@Autowired
	public  OptionService optionService;
	
	@PostMapping("/createPoll")
	public String creatingPoll(@Valid @RequestBody Question question) {
		questionService.createPoll(question);
		return "poll Added Successfully";
	}


	@GetMapping("/{pollId}/getPoll")
	public Question getPollById(@PathVariable("pollId") int id) {
		
		return questionService.getPollById(id);
	}
	
	@GetMapping("/getExistingPolls")
	public List<Question> getExistingPolls(){
		return questionService.getExistingPolls();
	}
	
	@GetMapping("/{questionId}/{optionId}/optionExist")
	public boolean isOptionExist(@PathVariable("questionId") int id,@PathVariable("optionId") int id2) {
	
		try {
		Options option=optionService.findById(id2);
		Question question=questionService.getPollById(id);
		List<Options> options=question.getOptions();
		if(options.contains(option)) {
			return true;
		}else {
			return false;
		}
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}
	
//	@GetMapping("/contestant/{id}/{id2}")
//	public Contestants getContestants(@PathVariable int id,@PathVariable int id2) {
//	return positionService.findContestantNameByPositionId(id,id2);
//	}
	


	@DeleteMapping("/{id}/deletePoll")
	public String deletePollById(@PathVariable int id) {
		try {
		 questionService.deleteById(id);
		 return "Deleted";
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}

	
}
