package com.myapp.votingsystem.repositry;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.myapp.votingsystem.model.Votes;

public interface VotesRepository extends JpaRepository<Votes, Integer> {

	List<Votes> findByQuestionId(int id);

	
}
