package com.myapp.votingsystem.repositry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.myapp.votingsystem.model.Question;

public interface QuestionRepositry extends JpaRepository<Question, Integer>{

	void deleteById(int id);

}
