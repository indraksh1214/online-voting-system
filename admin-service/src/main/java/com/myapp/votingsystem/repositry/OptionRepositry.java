package com.myapp.votingsystem.repositry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.myapp.votingsystem.model.Options;

public interface OptionRepositry extends JpaRepository<Options, Integer> {

	
}
