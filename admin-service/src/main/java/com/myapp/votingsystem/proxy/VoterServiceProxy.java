package com.myapp.votingsystem.proxy;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name="voter-service",url="localhost:8000")
public interface VoterServiceProxy {

	
	@GetMapping("/{questionId}/{optionId}/calculatingVotes")
	public long calculatingVotes(@PathVariable("questionId") int id, @PathVariable("optionId") int id1);
	
	@GetMapping("/getComments/{questionId}/{optionId}")
	public String[] getComments(@PathVariable("questionId") int questionId,
												@PathVariable("optionId") int optionId);

	
	@PostMapping("/{questionId}/{optionId}/{voterId}/{comment}/polling")
	public String conductingPolling(@PathVariable("questionId") int questionId, @PathVariable("optionId") int optionId,
			@PathVariable("voterId") int voterId, @PathVariable("comment") String comment);
	
}