package com.myapp.votingsystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myapp.votingsystem.model.Options;
import com.myapp.votingsystem.repositry.OptionRepositry;

@Service
public class OptionService {
	
	@Autowired
	private OptionRepositry optionRepositry;


	public Options findById(int id) {
		 List<Options> options=optionRepositry.findAll();
		 Options option=null;
		 for(Options op:options) {
			 if(op.getId()==id)
				 option=op;
		 }
		return option;
		
	}
	
	public List<Options> getAllOptions() {
		
		return optionRepositry.findAll();
		
	}

	public String deleteById(int id) {
		
			optionRepositry.deleteById(id);
		return "Deleted";
	
	}

	public Options save(Options option1) {
		// TODO Auto-generated method stub
		return optionRepositry.save(option1);
	}
}
