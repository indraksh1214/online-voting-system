package com.myapp.votingsystem.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myapp.votingsystem.model.Votes;
import com.myapp.votingsystem.repositry.VotesRepository;

@Service
public class VotesService {
	
	@Autowired
	private VotesRepository votesRepository;

	public String findByQuestionId(int questionId) {
		List<Votes> votes=votesRepository.findByQuestionId(questionId);
		HashMap<Integer,Long> result=new HashMap<Integer,Long>();
		for(int i=0;i<votes.size();i++)
		{
			Votes vote=votes.get(i);
			result.put(vote.getOptionId(),vote.getVotes());
		}
		Long winner = Collections.max(result.values());
		List<Integer> keys = new ArrayList<>();
		for (Entry<Integer, Long> entry : result.entrySet()) {
		    if (entry.getValue()==winner) {
		        keys.add(entry.getKey());
		    }
		}
		return votes+"\n"+"THE WINNER OpionId IS-->"+keys+" WITH THE VOTES--->"+winner;
	}

	public Votes saveVotes(Votes votes) {
		
		return votesRepository.save(votes);
	}
	
}

