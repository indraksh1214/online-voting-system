package com.myapp.votingsystem.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myapp.votingsystem.model.Question;
import com.myapp.votingsystem.repositry.QuestionRepositry;

@Service
public class QuestionService {

	@Autowired
	private QuestionRepositry questionRepositry;

	

	public Question createPoll(Question question) {

		
		
		return questionRepositry.save(question);
	}

	public Question getPollById(int id) {
		
	List<Question> questions=questionRepositry.findAll();
	Question question=null;
	for(Question que:questions) {
		if(que.getId()==id)
			question=que;
	}
	return question;	

	}


	public String deleteById(int id) {
			questionRepositry.deleteById(id);
			return "Deleted";

	}
	
	public List<Question> getExistingPolls() {

		return questionRepositry.findAll();
	}
}
