package com.myapp.votingservice.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity(name="polling")
@Table(name="polling")
@Component
public class Polling {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id; 
	private int questionId;
	private int optionId;
	private int voterId;
	private String comment;
	private int vote;
	
	public Polling(int questionId, int optionId, int voterId, String comment, int vote) {
		super();
		this.questionId = questionId;
		this.optionId = optionId;
		this.voterId = voterId;
		this.comment = comment;
		this.vote = vote;
	}
	
	public Polling(int id,int questionId, int optionId, int voterId, String comment, int vote) {
		super();
		this.id = id;
		this.questionId = questionId;
		this.optionId = optionId;
		this.voterId = voterId;
		this.comment = comment;
		this.vote = vote;
	}
	
	public Polling() {
	
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id=id;
	}
	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
	public int getOptionId() {
		return optionId;
	}
	public void setOptionId(int optionId) {
		this.optionId = optionId;
	}
	public int getVoterId() {
		return voterId;
	}
	public void setVoterId(int voterId) {
		this.voterId = voterId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int getVote() {
		return vote;
	}
	public void setVote(int vote) {
		this.vote = vote;
	}

	

	
	

}