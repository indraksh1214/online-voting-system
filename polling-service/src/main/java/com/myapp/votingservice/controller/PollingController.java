package com.myapp.votingservice.controller;



import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myapp.votingservice.Exception.FeignClientException;
import com.myapp.votingservice.Exception.NotFoundException;
import com.myapp.votingservice.entity.Polling;
import com.myapp.votingservice.proxy.AdminServiceProxy;
import com.myapp.votingservice.service.PollingService;

import feign.FeignException;

@RestController
public class PollingController {

	

	@Autowired
	private PollingService polllingService;

	

	@GetMapping("/{questionId}/{voterId}")
	public boolean voterStatus(@PathVariable("questionId") int questionId, @PathVariable("voterId") int voterId) {
		
		return polllingService.voterStatus(questionId, voterId);
		
	}

	@PostMapping("/{questionId}/{optionId}/{voterId}/{comment}/polling")
	public String conductingPolling(@PathVariable("questionId") int questionId, @PathVariable("optionId") int optionId,
			@PathVariable("voterId") int voterId, @PathVariable("comment") String comment) {

		return polllingService.pollingStatus(questionId,optionId,voterId,comment);
		

	}
		
	@GetMapping("/{questionId}/{optionId}/calculatingVotes")
	public long calculatingVotes(@PathVariable("questionId") int questionId, @PathVariable("optionId") int optionId) {

			return polllingService.totalVotesGivenByQuestionIdAndOptionId(questionId , optionId);
			
	}

	@GetMapping("/getComments/{questionId}/{optionId}")
	public String[] getComments(@PathVariable("questionId") int questionId, @PathVariable("optionId") int optionId) {
		
		return polllingService.findCommentByQuestionIdAndOptionId(questionId, optionId);
		
	}
	
	
}
