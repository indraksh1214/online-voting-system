package com.myapp.votingservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.myapp.votingservice.entity.Polling;

public interface PollingRepositry extends JpaRepository<Polling, Integer>{

	Polling findVoteByQuestionIdAndVoterId(int questionId, int voterId);

	@Query("select count(vote) from polling where questionId=?1 and optionId=?2")
	Long countVoteByQuestionIdAndOptionId(int id, int id1);
	
	@Query("select comment from polling where questionId=?1 and optionId=?2")
	String[] findCommentByQuestionIdAndOptionId(int id, int id1);

}
