package com.myapp.votingservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.myapp.votingservice.entity.User;

public interface UserRepositry extends JpaRepository<User, Integer> {

	User findByUserName(String userName);

	boolean existsByUserName(String userName);

	User findByEmailId(String emailId);

	User getUserById(Long userId);

}
