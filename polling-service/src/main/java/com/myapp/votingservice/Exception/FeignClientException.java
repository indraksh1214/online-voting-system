package com.myapp.votingservice.Exception;

import feign.FeignException;

public class FeignClientException extends FeignException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2154964462323901695L;

	public FeignClientException(int status, String message) {
		super(status, message);
		// TODO Auto-generated constructor stub
	}

}
