package com.myapp.votingservice.Exception;

import java.util.NoSuchElementException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;



@ControllerAdvice
public class MyExceptionController extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(EmptyInputException.class)
	public ResponseEntity<String> handleException(EmptyInputException emptyInputException)
	{
		return new  ResponseEntity<String>("please check your input entries",HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(NoSuchElementException.class)
	public ResponseEntity<String> NoSuchElementException(NoSuchElementException noSuchElementException)
	{
		return new  ResponseEntity<String>("elements are not present with your credentials",HttpStatus.NOT_FOUND);
	}
		
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			org.springframework.http.HttpHeaders headers, HttpStatus status, WebRequest request) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Object>("plese change method type ",HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(FeignClientException.class)
	public ResponseEntity<String> FeignClientException(FeignClientException feignClientException)
	{
		return new  ResponseEntity<String>("check your feign client class",HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<String> NotFoundException(NotFoundException notFoundException)
	{
		return new  ResponseEntity<String>("elements are not found check your credentials",HttpStatus.NOT_FOUND);
	}
	
}
