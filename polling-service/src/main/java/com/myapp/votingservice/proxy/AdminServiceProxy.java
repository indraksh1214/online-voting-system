package com.myapp.votingservice.proxy;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;



@FeignClient(name="admin-service",url="localhost:8001")
public interface AdminServiceProxy {
	

	@GetMapping("/{questionId}/{optionId}/optionExist")
	public boolean isOptionExist(@PathVariable("questionId") int id,@PathVariable("optionId") int id2);
}