package com.myapp.votingservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myapp.votingservice.Exception.FeignClientException;
import com.myapp.votingservice.Exception.NotFoundException;
import com.myapp.votingservice.entity.Polling;
import com.myapp.votingservice.proxy.AdminServiceProxy;
import com.myapp.votingservice.repository.PollingRepositry;

import feign.FeignException;

@Service
public class PollingService {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AdminServiceProxy adminServiceProxy;
	
	@Autowired
	private PollingRepositry pollingRepositry;

	public boolean voterStatus(int questionId, int voterId) {
		Polling polling=pollingRepositry.findVoteByQuestionIdAndVoterId(questionId,voterId);
		if(polling==null)
			return false;
		else
		return true;
		
	}


	public Polling save(Polling polling) {
		return pollingRepositry.save(polling);
		
	}


	public String[] findCommentByQuestionIdAndOptionId(int questionId, int optionId) {
	
		try {
			if(adminServiceProxy.isOptionExist(questionId, optionId))
			{
				String[] comments=	pollingRepositry.findCommentByQuestionIdAndOptionId(questionId, optionId);
				
				return comments;
				
				
				}else {
					throw new NotFoundException();
				}		
		}catch (FeignException e) {
			throw new FeignClientException(0,"check your entries");
		}	
	}

	public String pollingStatus(int questionId,int optionId,int voterId,String comment) {
		
		try {
		boolean isExist = adminServiceProxy.isOptionExist(questionId, optionId);
		boolean isUserExist = userService.isUserExist(voterId);
		boolean status = voterStatus(questionId, voterId);

		if (isExist) {
			if (isUserExist) {
				if (status) {
					return "Already voted";
				} else {
					int vote = 1;
					Polling polling = new Polling(questionId,optionId,voterId,comment,vote);
					pollingRepositry.save(polling);
					return "succefully voted";
				}
			} else {
				return "Check your voterId";
			}
		} else {
			return "check question and option id";
		}
		}catch(FeignException e){
			throw new FeignClientException(0,"check your entries");
		}
		
	}

	public long totalVotesGivenByQuestionIdAndOptionId(int questionId, int optionId) {
		try {
			if(adminServiceProxy.isOptionExist(questionId , optionId))
			return pollingRepositry.countVoteByQuestionIdAndOptionId(questionId,optionId);
			else
				return -1;
		}catch (FeignException e) {
			throw new FeignClientException(0,"check your entries");
		}
	}
		
}
