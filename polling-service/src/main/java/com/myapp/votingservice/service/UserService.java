package com.myapp.votingservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myapp.votingservice.entity.User;
import com.myapp.votingservice.repository.UserRepositry;

@Service
public class UserService {
	


	@Autowired
	public UserRepositry userRepositry;

	public Boolean isUserExist(int id) {
		List<User> users= userRepositry.findAll();
		User user=null;
		for(User us:users) {
			if(us.getId()==id)
				user=us;
		}
		if (user == null)
			return false;
		else
		return true;
	}

}
