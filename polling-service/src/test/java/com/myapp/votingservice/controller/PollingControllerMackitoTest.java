package com.myapp.votingservice.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import com.myapp.votingservice.proxy.AdminServiceProxy;
import com.myapp.votingservice.service.PollingService;

@TestMethodOrder(OrderAnnotation.class)
@ComponentScan(basePackages = "com.myapp.votingsystem")
@SpringBootTest(classes = { PollingControllerMackitoTest.class })
public class PollingControllerMackitoTest {


	@Mock
	private PollingService polllingService;

	@Mock
	private AdminServiceProxy adminServiceProxy;
	
	@InjectMocks
	private PollingController pollingController;
	
	
	@Test
	@Order(1)
	public void test_voterStatus() {
		
		int questionId=1;
		int voterId=1;
		when(polllingService.voterStatus(questionId, voterId)).thenReturn(true);
		assertEquals(true,pollingController.voterStatus(questionId, voterId));
		
		when(polllingService.voterStatus(questionId, voterId)).thenReturn(false);
		assertEquals(false,pollingController.voterStatus(questionId, voterId));
		
		//when(polllingService.findVoteByQuestionIdAndVoterId(questionId, voterId)).thenThrow(new NotFoundException());
		
//		Exception exception=assertThrows(NotFoundException.class,()->{pollingController.voterStatus(questionId, voterId);});
//		
//		assertEquals("elements are not found check your credentials",exception.getMessage());
	}
	
	@Test
	@Order(2)
	public void test_conductingPolling() {
			
		int questionId=1;
		int optionId=1;
		int voterId=1;
		String comment="hi";
	when(polllingService.pollingStatus(questionId,optionId,voterId,comment)).thenReturn("voted");
	assertEquals("voted",pollingController.conductingPolling(questionId,optionId,voterId,comment));
	}
	
	@Test
	@Order(3)
	public void test_getComments() {
		
//		String[] comments= {"hi","hello","namasthe"};
//		int questionId=1;
//		int optionId=1;
//		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenReturn(true);
//		
//		when(polllingService.findCommentByQuestionIdAndOptionId(questionId, optionId)).thenReturn(comments);
//		
//		assertEquals(3,pollingController.getComments(questionId, optionId).length);
//	
//		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenReturn(false);
//		assertEquals(1,pollingController.getComments(questionId, optionId).length);
//		
////		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenThrow(new FeignClientException(0,"check your entries"));
//		Exception exception=assertThrows(FeignClientException.class,()->{pollingController.getComments(questionId, optionId);});
//		assertEquals("elements are not present with your credentials",exception.getMessage());
		
		String[] comments= {"hi","hello","namasthe"};
		int questionId=1;
		int optionId=1;
		when(polllingService.findCommentByQuestionIdAndOptionId(questionId, optionId)).thenReturn(comments);
		assertEquals(comments,pollingController.getComments(questionId, optionId));
		
		//when(polllingService.findCommentByQuestionIdAndOptionId(questionId, optionId)).thenThrow(new NotFoundException());
		//Exception exception=assertThrows(NotFoundException.class,()->{pollingController.getComments(questionId, optionId);});
		//assertEquals("elements are not found check your credentials",exception.getMessage());
	
		//when(polllingService.findCommentByQuestionIdAndOptionId(questionId, optionId)).thenThrow(new FeignClientException(0,"check your entries"));
		//Exception exception1=assertThrows(FeignClientException.class,()->{pollingController.getComments(questionId, optionId);});
		//assertEquals("check your feign client class",exception1.getMessage());
	}
	
	@Test
	@Order(4)
	public void test_calculatingVotes( ) {
	
//		int questionId=1;
//		int optionId=1;
//		when(adminServiceProxy.isOptionExist(questionId , optionId)).thenReturn(true);
//		when(polllingService.countVoteByQuestionIdAndOptionId(questionId , optionId)).thenReturn((long)10);
//		assertEquals((long)10,pollingController.calculatingVotes(questionId, optionId));
//		
//		when(adminServiceProxy.isOptionExist(questionId , optionId)).thenReturn(false);
//		assertEquals(-1,pollingController.calculatingVotes(questionId, optionId));
//		
////		when(adminServiceProxy.isOptionExist(questionId , optionId)).thenThrow(new FeignClientException(0,"check your entries"));
//		Exception exception=assertThrows(FeignClientException.class,()->{pollingController.calculatingVotes(questionId, optionId);});
//		assertEquals("elements are not present with your credentials",exception.getMessage());
		int questionId=1;
		int optionId=1;
		long vote=10;
	when(polllingService.totalVotesGivenByQuestionIdAndOptionId(questionId , optionId)).thenReturn(vote);
	assertEquals(vote,pollingController.calculatingVotes(questionId, optionId));
	}
}
