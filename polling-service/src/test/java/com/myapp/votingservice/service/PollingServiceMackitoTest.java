package com.myapp.votingservice.service;

import static org.junit.Assert.assertThrows;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import com.myapp.votingservice.Exception.FeignClientException;
import com.myapp.votingservice.Exception.NotFoundException;
import com.myapp.votingservice.entity.Polling;
import com.myapp.votingservice.proxy.AdminServiceProxy;
import com.myapp.votingservice.repository.PollingRepositry;

@TestMethodOrder(OrderAnnotation.class)
@ComponentScan("com.myapp.votingservice")
@SpringBootTest(classes = {PollingServiceMackitoTest.class})
public class PollingServiceMackitoTest {

	@Mock
	private PollingRepositry pollingRepositry;
	
	@Mock
	private UserService userService;
	
	@Mock
	private AdminServiceProxy adminServiceProxy;
	
	@InjectMocks
	private PollingService pollingService;
	
	@Test
	@Order(1)
	public void test_voterStatus() {
	
		Polling polling=new Polling(1,1,1,11,"hello",1);
		int questionId=1;
		int voterId=1;
		when(pollingRepositry.findVoteByQuestionIdAndVoterId(questionId,voterId)).thenReturn(polling);
		assertEquals(true,pollingService.voterStatus(questionId, voterId));
		
		when(pollingRepositry.findVoteByQuestionIdAndVoterId(questionId,voterId)).thenReturn(null);
		assertEquals(false,pollingService.voterStatus(questionId, voterId));
		
	}
	

	
	@Test
	@Order(2)
	public void test_save() {
		
		Polling polling=new Polling();
		polling.setId(3);
		polling.setQuestionId(1);
		polling.setOptionId(1);
		polling.setVoterId(13);
		polling.setComment("hello");
		polling.setVote(1);
		when(pollingRepositry.save(polling)).thenReturn(polling);
		
		assertEquals(3,pollingService.save(polling).getId());
		assertEquals(1,pollingService.save(polling).getQuestionId());
		assertEquals(1,pollingService.save(polling).getOptionId());
		assertEquals(13,pollingService.save(polling).getVoterId());
		assertEquals("hello",pollingService.save(polling).getComment());
		assertEquals(1,pollingService.save(polling).getVote());
	}
	
	@Test
	@Order(3)
	public void test_findCommentByQuestionIdAndOptionId() {
		
		Polling polling=new Polling(1,1,11,"hello",1);
		Polling polling1=new Polling(1,1,12,"hello",1);
		Polling polling2=new Polling(1,1,13,"hello",1);
		
		String[] comments= {polling.getComment(),polling1.getComment(),polling2.getComment()};
		int questionId=1;
		int optionId=1;
		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenReturn(true);
		when(pollingRepositry.findCommentByQuestionIdAndOptionId(questionId, optionId)).thenReturn(comments);
		assertEquals( comments,  pollingService.findCommentByQuestionIdAndOptionId(questionId,optionId));
		
		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenReturn(false);
		Exception exception1=assertThrows(NotFoundException.class,()->{pollingService.findCommentByQuestionIdAndOptionId(questionId, optionId);});
		assertEquals(null,exception1.getMessage());
		
		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenThrow(new FeignClientException(0,"check your entries"));
		Exception exception=assertThrows(FeignClientException.class,()->{pollingService.findCommentByQuestionIdAndOptionId(questionId, optionId);});
		assertEquals("check your entries",exception.getMessage());
		
	
	}
	
	@Test
	@Order(4)
	public void pollingStatus() {
		
		Polling polling1=new Polling(1,1,1,1,"hi",1);
		int questionId=1;
		int optionId=1;
		int voterId=1;
		String comment="hi";
		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenReturn(true);
		when(userService.isUserExist(voterId)).thenReturn(true);
		
		//when(pollingService.voterStatus(questionId, voterId)).thenReturn(false);
		when(pollingRepositry.findVoteByQuestionIdAndVoterId(questionId,voterId)).thenReturn(null);
		
		when(pollingRepositry.save(polling1)).thenReturn(polling1);
		
		assertEquals("succefully voted",pollingService.pollingStatus(questionId,optionId,voterId,comment));
	
		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenReturn(true);
		when(userService.isUserExist(voterId)).thenReturn(true);
		when(pollingRepositry.findVoteByQuestionIdAndVoterId(questionId,voterId)).thenReturn(polling1);
		//when(pollingService.voterStatus(questionId, voterId)).thenReturn(true);
	
		
		assertEquals("Already voted",pollingService.pollingStatus(questionId,optionId,voterId,comment));
		
		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenReturn(false);
		when(userService.isUserExist(voterId)).thenReturn(true);
		assertEquals("check question and option id",pollingService.pollingStatus(questionId,optionId,voterId,comment));
		
		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenReturn(true);
		when(userService.isUserExist(voterId)).thenReturn(false);
		assertEquals("Check your voterId",pollingService.pollingStatus(questionId,optionId,voterId,comment));
		
		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenThrow(new FeignClientException(0,"check your entries"));
		Exception exception=assertThrows(FeignClientException.class,()->{pollingService.pollingStatus(questionId,optionId,voterId,comment);});
		assertEquals("check your entries",exception.getMessage());
	
	}
	@Test
	@Order(5)
	public void totalVotesGivenByQuestionIdAndOptionId() {
		
		//Polling polling=new Polling(1,1,1,11,"hello",1);
		int questionId=1;
		int optionId=1;
		long vote=10;
		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenReturn(true);
		when(pollingRepositry.countVoteByQuestionIdAndOptionId(questionId,optionId)).thenReturn(vote);
		assertEquals(vote,pollingService.totalVotesGivenByQuestionIdAndOptionId(questionId,optionId));	
		
		long vote1=-1;
		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenReturn(false);
		when(pollingRepositry.countVoteByQuestionIdAndOptionId(questionId,optionId)).thenReturn(vote1);
		assertEquals(vote1,pollingService.totalVotesGivenByQuestionIdAndOptionId(questionId,optionId));	

		when(adminServiceProxy.isOptionExist(questionId, optionId)).thenThrow(new FeignClientException(0,"check your entries"));
		Exception exception=assertThrows(FeignClientException.class,()->{pollingService.totalVotesGivenByQuestionIdAndOptionId(questionId,optionId);});
		assertEquals("check your entries",exception.getMessage());
	}
}
