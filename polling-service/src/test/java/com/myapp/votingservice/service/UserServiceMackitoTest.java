package com.myapp.votingservice.service;

import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import com.myapp.votingservice.repository.UserRepositry;
import com.myapp.votingservice.entity.User;
@TestMethodOrder(OrderAnnotation.class)
@ComponentScan("com.myapp.votingservice")
@SpringBootTest(classes = {UserServiceMackitoTest.class})
public class UserServiceMackitoTest {

	@Mock
	private UserRepositry userRepositry;
	
	@InjectMocks
	private UserService userService;
	
	@Test
	@Order(1)
	public void test_isUserExist() {
		User user=new User(1,"indra","indra123","indra@gmail.com","admin");
		User user1=new User(2,"kamu","kamu123","kamu@gmail.com","voter");
		List<User> users=new ArrayList<User>();
		users.add(user);
		users.add(user1);
		
		int id=1;
		when( userRepositry.findAll()).thenReturn(users);
		assertEquals(true,userService.isUserExist(id));
		
		int id1=3;
		when( userRepositry.findAll()).thenReturn(users);
		assertEquals(false,userService.isUserExist(id1));
		}
}
