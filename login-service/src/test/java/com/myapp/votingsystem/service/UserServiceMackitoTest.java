package com.myapp.votingsystem.service;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpStatus;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.myapp.votingsystem.config.JwtTokenProvider;
import com.myapp.votingsystem.exception.CustomException;
import com.myapp.votingsystem.exception.UserAlreadyExistsException;
import com.myapp.votingsystem.model.User;
import com.myapp.votingsystem.repo.UserRepositry;
//@TestMethodOrder(OrderAnnotation.class)
//@ComponentScan("com.myapp.votingsystem")
//@SpringBootTest(classes = {UserServiceMackitoTest.class})
@WebMvcTest(UserService.class)
public class UserServiceMackitoTest {

	@MockBean
	private UserRepositry userRepositry;
	
	@Autowired
	private UserService userService;
	
	@MockBean
	public PasswordEncoder passwordEncoder;

	@MockBean
	private AuthenticationManager authenticationManager;
	
	@MockBean
	private JwtTokenProvider jwtTokenProvider;
		
	@Test
	@Order(1)
	public void test_getUsers() {
	
		User user=new User(1,"indra","indra123","indra@gmail.com","admin");
		User user1=new User(2,"kamu","kamu123","kamu@gmail.com","voter");
		List<User> users=new ArrayList<User>();
		users.add(user);
		users.add(user1);
		
		when(userRepositry.findAll()).thenReturn(users);
		assertEquals(2,  userService.getUsers().size());
		
	}
	
	@Test
	@Order(2)
	public void test_isUserExist() {
		User user=new User(1,"indra","indra123","indra@gmail.com","admin");
		User user1=new User(2,"kamu","kamu123","kamu@gmail.com","voter");
		List<User> users=new ArrayList<User>();
		users.add(user);
		users.add(user1);
		
		int id=1;
		when( userRepositry.findAll()).thenReturn(users);
		assertEquals(true,userService.isUserExist(id));
		
		int id1=5;
		when( userRepositry.findAll()).thenReturn(users);
		assertEquals(false,userService.isUserExist(id1));
	}
	
	@Test
	@Order(3)
	public void test_findByUserName() {
	
		User user=new User();
		
		user.setId(1);
		user.setUserName("indra");
		user.setPassword("indra123");
		user.setEmailId("indra@gmail.com");
		user.setRole("admin");
		
		String userName="indra";
		when(userRepositry.findByUserName(userName)).thenReturn(user);
		assertEquals(1, userService.findByUserName(userName).getId());
		assertEquals("indra", userService.findByUserName(userName).getUserName());
		assertEquals("indra123", userService.findByUserName(userName).getPassword());
		assertEquals("admin", userService.findByUserName(userName).getRole());
		assertEquals("indra@gmail.com", userService.findByUserName(userName).getEmailId());
		assertEquals(user, userService.findByUserName(userName));
		
		when(userRepositry.findByUserName(userName)).thenReturn(null);
		assertEquals(null, userService.findByUserName(userName));
	}
	
	
	@Test
	@Order(5)
	public void test_saveUser() {
		//User user=new User(1,"indra","indra123","indra@gmail.com","admin");
		String password="aW5kcmExMjM=";
		User user=new User();
		
		user.setId(1);
		user.setUserName("indra");
		user.setPassword("indra123");
		user.setEmailId("indra@gmail.com");
		user.setRole("admin");
		
		when(userRepositry.findByEmailId(user.getEmailId())).thenReturn(null);
		when(passwordEncoder.encode(user.getPassword())).thenReturn(password);
		when(userRepositry.save(user)).thenReturn(user);
		assertEquals(user,userService.saveUser(user));
		assertEquals("indra123",userService.saveUser(user).getPassword());
		
		when(userRepositry.findByEmailId(user.getEmailId())).thenReturn(user);
		assertThrows(UserAlreadyExistsException.class,()->{userService.saveUser(user);});
	}

	@Test
	@Order(5)
	public void test_signin() {
		
		String userName="kamu";
		String password="kamu";
		when(authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, password))).thenReturn(null);
		String token="qwertyu.456yhjkl.78uygbn";
		when(jwtTokenProvider.createToken(userName)).thenReturn(token);
		assertEquals(token,userService.signin(userName, password));
		
//		when(authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, password)))
//		.thenThrow(new CustomException("Invalid username/password supplied",HttpStatus.UNPROCESSABLE_ENTITY));
	}
	
	@Test
	@Order(6)
	public void test_signup( ) {
		
		User user=new User(1,"indra","indra@123","indra@gmail.com","admin");
		
		String password=passwordEncoder.encode(user.getPassword());
		when(userRepositry.existsByUserName(user.getUserName())).thenReturn(true);
		when(passwordEncoder.encode(user.getPassword())).thenReturn(password);
		when(userRepositry.save(user)).thenReturn(user);
	}
	
	@Test
	@Order(7)
	public void test_isVoterEnteredProperVoterId( ) {
		
		User user=new User(1,"indra","indra@123","indra@gmail.com","admin");
		int voterId=1;
		when(userRepositry.getUserById(voterId)).thenReturn(user);
		String name="indra";
		when(jwtTokenProvider.getUserCheckName()).thenReturn(name);
		assertEquals(true,userService.isVoterEnteredProperVoterId(voterId));
		
		when(userRepositry.getUserById(voterId)).thenReturn(user);
		String name1="kindra";
		when(jwtTokenProvider.getUserCheckName()).thenReturn(name1);
		assertEquals(false,userService.isVoterEnteredProperVoterId(voterId));
	}
	
	@Test
	@Order(8)
	public void test_deleteUser() {
		
		User user=new User(1,"indra","indra@123","indra@gmail.com","admin");
		int voterId=1;
		when(userRepositry.getUserById(voterId)).thenReturn(user);
		
		assertEquals("Deleted",userService.deleteUser(voterId));
		verify(userRepositry,times(1)).deleteById(voterId);
		
		
		when(userRepositry.getUserById(voterId)).thenReturn(null);
		assertEquals("check your entry",userService.deleteUser(voterId));
	}
	
	@Test
	@Order(9)
	public void test_getUserById() {
	
		User user=new User(1,"indra","indra123","indra@gmail.com","admin");
		int voterId=1;
		when(userRepositry.getUserById(voterId)).thenReturn(user);
		assertEquals(user,userService.getUserById(voterId));
		
		when(userRepositry.getUserById(voterId)).thenReturn(null);
		assertEquals(null,userService.getUserById(voterId));
	}
}