package com.myapp.votingsystem.controller;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.Order;

import com.myapp.votingsystem.exception.NotFoundException;
import com.myapp.votingsystem.model.User;

@TestMethodOrder(OrderAnnotation.class)
@ComponentScan(basePackages = "com.myapp.votingsystem")
@SpringBootTest(classes = { VoterControllerMackitoTest.class })
public class VoterControllerMackitoTest {

	
	@Mock
	public com.myapp.votingsystem.service.UserService userService;
	
	@Mock
	public com.myapp.votingsystem.proxy.Proxy proxy;
	
	@InjectMocks
	private VoterController voterController;
	
	
	
	@Test
	@Order(1)
	public void test_conductingPolling() {
		
		//User user=new User(1,"indra","indra123","indra@gmail.com","admin");
		String message="voted";
		int voterId=1;
		int questionId=1;
		int optionId=1;
		String comment="hi";
		when(userService.isVoterEnteredProperVoterId(voterId)).thenReturn(true);
		when(proxy.conductingPolling(questionId, optionId, voterId, comment)).thenReturn(message);
		assertEquals(message,voterController.conductingPolling(questionId, optionId, voterId, comment));
		
		String message1="Enter Proper VoterId";
		when(userService.isVoterEnteredProperVoterId(voterId)).thenReturn(false);
		assertEquals(message1,voterController.conductingPolling(questionId, optionId, voterId, comment));
	
		when(userService.isVoterEnteredProperVoterId(voterId)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{voterController.conductingPolling(questionId, optionId, voterId, comment);});
	}
	
	@Test
	@Order(2)
	public void test_finById() {
		
		User user=new User(1,"indra","indra123","indra@gmail.com","admin");
		int id=1;
		when(userService.getUserById(id)).thenReturn(user);
		assertEquals(user,voterController.finById(id));
		
		when(userService.getUserById(id)).thenReturn(null);
		assertEquals(null,voterController.finById(id));
		
		when(userService.getUserById(id)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{voterController.finById(id);});
	}

	@Test
	@Order(3)
	public void test_deleteUser() {
		
		int id=1;
		String message="Deleted";
		when(userService.deleteUser(id)).thenReturn(message);
		assertEquals(message,voterController.deleteUser(id));
		
		when(userService.deleteUser(id)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{voterController.deleteUser(id);});
	}
	
	@Test
	@Order(4)
	public void test_getUsers() {
		
		User user=new User(1,"indra","indra123","indra@gmail.com","admin");
		User user1=new User(2,"indra","indra123","kindra@gmail.com","voter");
		List<User> users=new ArrayList<>();
		users.add(user1);
		users.add(user);
		when(userService.getUsers()).thenReturn(users);
		assertEquals(2,voterController.getUsers().size());
		
		when(userService.getUsers()).thenReturn(null);
		assertEquals(null,voterController.getUsers());
	}
}
