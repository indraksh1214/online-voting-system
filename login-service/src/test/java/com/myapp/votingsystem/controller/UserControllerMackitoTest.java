package com.myapp.votingsystem.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.Order;

import com.myapp.votingsystem.model.User;
import com.myapp.votingsystem.service.UserService;

@TestMethodOrder(OrderAnnotation.class)
@ComponentScan(basePackages = "com.myapp.votingsystem")
@SpringBootTest(classes = { VoterControllerMackitoTest.class })
public class UserControllerMackitoTest {

	@Mock
	public UserService userService;
	
	@InjectMocks
	public UserController userController;
	
	@Test
	@Order(1)
	public void test_userRegistration() {
		
		User user=new User(1,"indra","indra123","indra@gmail.com","admin");
		
		String message="hi registered";
		when(userService.saveUser(user)).thenReturn(user);
	
	   assertEquals(message,userController.userRegistration(user));
	}
	
	@Test
	@Order(2)
	public void test_loginByUserName( ) {
		
		String userName="kamu";
		String password="kamu";
		String token="werty234cvdfgh.456erdfghuh.8ghcvbuio";
		when( userService.signin(userName, password)).thenReturn(token);
		assertEquals(token,userController.loginByUserName(userName, password));
		
		when( userService.signin(userName, password)).thenReturn("");
		assertEquals("token not generated",userController.loginByUserName(userName, password));
	}
}
