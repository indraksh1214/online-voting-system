package com.myapp.votingsystem.controller;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.Order;

import com.myapp.votingsystem.exception.NotFoundException;

@TestMethodOrder(OrderAnnotation.class)
@ComponentScan(basePackages = "com.myapp.votingsystem")
@SpringBootTest(classes = { VoterControllerMackitoTest.class })
public class ResultControllerMackitoTest {

	@Mock
	public com.myapp.votingsystem.proxy.Proxy proxy;
	
	@InjectMocks
	private ResultController resultController;
	
	@Test
	@Order(1)
	public void test_finalResult() {
		
		String message="winner is"+1;
		int questionId=1;
		when(proxy.finalResult(questionId)).thenReturn(message);
		assertEquals("winner is"+1,resultController.finalResult(questionId));
		
		when(proxy.finalResult(questionId)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{resultController.finalResult(questionId);});
	}
	
	@Test
	@Order(1)
	public void test_fetchComments() {
		
		String[] comments= {"hi","hello"};
		int questionId=1;
		int optionId=1;
		when( proxy.fetchComments(questionId, optionId)).thenReturn(comments);
		assertEquals(comments,  resultController.fetchComments(questionId, optionId));
		
		when( proxy.fetchComments(questionId, optionId)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{resultController.fetchComments(questionId, optionId);});
	}
}
