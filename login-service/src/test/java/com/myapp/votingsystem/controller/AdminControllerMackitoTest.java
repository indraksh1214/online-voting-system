package com.myapp.votingsystem.controller;

import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.netbeans.lib.cvsclient.commandLine.command.add;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.Order;

import com.myapp.votingsystem.exception.EmptyInputException;
import com.myapp.votingsystem.exception.NotFoundException;
import com.myapp.votingsystem.model.Options;
import com.myapp.votingsystem.model.Question;

@TestMethodOrder(OrderAnnotation.class)
@ComponentScan(basePackages = "com.myapp.votingsystem")
@SpringBootTest(classes = { AdminControllerMackitoTest.class })
public class AdminControllerMackitoTest {

	@Mock
	private com.myapp.votingsystem.proxy.Proxy proxy;
	
	@InjectMocks
	private AdminController adminController;
	
	@Test
	@Order(1)
	public void test_creatingPoll() {
		
		List<Options> option = new ArrayList<Options>(Arrays.asList(new Options(1, "YES"), new Options(2, "NO")));
		Question question = new Question(1, "Is Lockdown Neccessory", option);
		
		String message="poll added";
		when(proxy.creatingPoll(question)).thenReturn(message);
		assertEquals( "poll added",  adminController.creatingPoll(question));
		
		when(proxy.creatingPoll(question)).thenThrow(new EmptyInputException());
		assertThrows(EmptyInputException.class,()->{adminController.creatingPoll(question);});
	}
	
	@Test
	@Order(2)
	public void test_getPollById() {
		
		List<Options> option = new ArrayList<Options>(Arrays.asList(new Options(1, "YES"), new Options(2, "NO")));
		Question question = new Question(1, "Is Lockdown Neccessory", option);
	
		int pollId=1;
		when(proxy.getPollById(pollId)).thenReturn(question);
		assertEquals(1,adminController.getPollById(pollId).getId());
		assertEquals("Is Lockdown Neccessory",adminController.getPollById(pollId).getQuestionName());
		assertEquals(option,adminController.getPollById(pollId).getOptions());
		assertEquals(question,adminController.getPollById(pollId));
	
		when(proxy.getPollById(pollId)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{adminController.getPollById(pollId);});
	}
	
	@Test
	@Order(3)
	public void test_getOption() {
		
		Options option=new Options(1,"YES");
		
		int id=1;
		when(proxy.getOption(id)).thenReturn(option);
		assertEquals(1,adminController.getOption(id).getId());
		assertEquals("YES",adminController.getOption(id).getOptionName());
		assertEquals(option,adminController.getOption(id));
		
		when(proxy.getOption(id)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{adminController.getOption(id);});
	}
	
	@Test
	@Order(4)
	public void test_calculatingVotesForEachOption() {
		
		String message="saved";
		int questionId=1;
		int optionId=1;
		when(proxy.calculatingVotesForEachOption(questionId, optionId)).thenReturn(message);
		 assertEquals( "saved", adminController.calculatingVotesForEachOption(questionId, optionId));
		 
		 when(proxy.calculatingVotesForEachOption(questionId, optionId)).thenThrow(new NotFoundException());
		 assertThrows(NotFoundException.class,()->{adminController.calculatingVotesForEachOption(questionId, optionId);});
	}
	
	@Test
	@Order(5)
	public void test_getExistingPolls() {
		
		List<Options> option = new ArrayList<Options>(Arrays.asList(new Options(1, "YES"), new Options(2, "NO")));
		Question question = new Question(1, "Is Lockdown Neccessory", option);
		
		List<Options> option1 = new ArrayList<Options>(Arrays.asList(new Options(1, "NIGHT CURFEW"), new Options(2, "TEMPORORY LOCKDOWN")));
		Question question1 = new Question(2, "which is best option", option1);
	
		List<Question> questions=new ArrayList<>();
		questions.add(question1);
		questions.add(question);
		
		when(proxy.getExistingPolls()).thenReturn(questions);
		assertEquals(2,adminController.getExistingPolls().size());
		
		when(proxy.getExistingPolls()).thenReturn(null);
		assertEquals(null,adminController.getExistingPolls());
	}
	
	@Test
	@Order(6)
	public void test_deletePollById() {
		
		int id=1;
		String message="Deleted";
		when(proxy.deletePollById(id)).thenReturn(message);
		assertEquals(message,adminController.deletePollById(id));
		
		when(proxy.deletePollById(id)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{adminController.deletePollById(id);});
	}
	
	@Test
	@Order(7)
	public void test_updateOption() {
		
		Options option=new Options(1,"YES");
		when(proxy.updateOption(option)).thenReturn(option);
		assertEquals(option,adminController.updateOption(option));
		
		when(proxy.updateOption(option)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{adminController.updateOption(option);});
	}
	
	@Test
	@Order(8)
	public void test_daleteOptions() {
		
		int id=1;
		String message="Deleted";
		when(proxy.daleteOption(id)).thenReturn(message);
		assertEquals(message,adminController.daleteOptions(id));
		
		when(proxy.daleteOption(id)).thenThrow(new NotFoundException());
		assertThrows(NotFoundException.class,()->{adminController.daleteOptions(id);});
		
	}
	
}
