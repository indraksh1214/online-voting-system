package com.myapp.votingsystem.exception;

public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7752575217005974410L;
	
	public NotFoundException() {
		
	}
}
