package com.myapp.votingsystem.exception;

import java.util.NoSuchElementException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ControllerAdvice
public class MyExceptionController  {
	
	@ExceptionHandler(EmptyInputException.class)
	public ResponseEntity<String> handleException(EmptyInputException emptyInputException)
	{
		return new  ResponseEntity<String>("please check your input entries",HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(NoSuchElementException.class)
	public ResponseEntity<String> handleNoSuchElementException(NoSuchElementException noSuchElementException)
	{
		return new  ResponseEntity<String>("elements are not present with your credentials",HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<String> handleMethodArgumentNotValidException(MethodArgumentNotValidException methodArgumentNotValidException){
		return new ResponseEntity<String>("Validation Error",HttpStatus.BAD_REQUEST);
	}
	
//	@Override
//	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
//			org.springframework.http.HttpHeaders headers, HttpStatus status, WebRequest request) {
//		// TODO Auto-generated method stub
//		return new ResponseEntity<Object>("plese change method type ",HttpStatus.NOT_FOUND);
//	}

	
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<String> NotFoundException(NotFoundException notFoundException)
	{
		return new  ResponseEntity<String>("Details not found with your input credentials",HttpStatus.NOT_FOUND);
	}
}
