package com.myapp.votingsystem.repo;

import org.springframework.data.jpa.repository.JpaRepository;


import com.myapp.votingsystem.model.User;

public interface UserRepositry extends JpaRepository<User, Integer> {

	User findByUserName(String username);

	User findByEmailId(String emailId);

	User getUserById(int userId);

	boolean existsByUserName(String userName);

}
