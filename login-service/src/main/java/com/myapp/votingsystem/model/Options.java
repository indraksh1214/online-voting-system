package com.myapp.votingsystem.model;

public class Options {
	
	private int id;
	private String optionName;
	 //private String comments
	
	public Options(int id, String optionName) {
		super();
		this.id = id;
		this.optionName = optionName;
	}

	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	
	
}