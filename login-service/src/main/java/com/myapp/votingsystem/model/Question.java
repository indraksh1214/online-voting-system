package com.myapp.votingsystem.model;



import java.util.List;

public class Question {
	
	
	private int id;
	
	private String questionName;
	
	
	private List<Options> options;

	public Question(int id, String questionName, List<Options> options) {
		super();
		this.id = id;
		this.questionName = questionName;
		this.options = options;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public List<Options> getOptions() {
		return options;
	}

	public void setOptions(List<Options> options) {
		this.options = options;
	}
	
	
}

