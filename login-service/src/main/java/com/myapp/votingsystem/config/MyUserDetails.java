package com.myapp.votingsystem.config;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.myapp.votingsystem.model.User;
import com.myapp.votingsystem.repo.UserRepositry;

@Service
public class MyUserDetails implements UserDetailsService {

  @Autowired
  private UserRepositry userRepositry;

  @Override
  public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
    final User user = userRepositry.findByUserName(userName);

    if (user == null) {
      throw new UsernameNotFoundException("User '" + userName + "' not found");
    }

    return org.springframework.security.core.userdetails.User//
        .withUsername(userName)//
        .password(user.getPassword())//
        .authorities(user.getRole())
        .accountExpired(false)//
        .accountLocked(false)//
        .credentialsExpired(false)//
        .disabled(false)//
        .build();
  }

}

