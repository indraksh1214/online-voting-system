package com.myapp.votingsystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myapp.votingsystem.exception.EmptyInputException;
import com.myapp.votingsystem.exception.NotFoundException;
import com.myapp.votingsystem.model.Options;
import com.myapp.votingsystem.model.Question;
import com.myapp.votingsystem.model.User;
import com.myapp.votingsystem.service.UserService;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	public com.myapp.votingsystem.proxy.Proxy proxy;
	
	@Autowired
	public UserService userService;
	
	@PostMapping("/createPoll")
	public String creatingPoll(@Valid @RequestBody Question question) {
		try {
		return proxy.creatingPoll(question);
		}catch(Exception e) {
			throw new EmptyInputException();
		}
	}

	@GetMapping("/{pollId}/getPoll")
	public Question getPollById(@PathVariable("pollId") int id) {
		try {
		return proxy.getPollById(id);
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}
	
	@GetMapping("/getExistingPolls")
	public List<Question> getExistingPolls(){
		return proxy.getExistingPolls();
	}
	
	@DeleteMapping("/{id}/deletePoll")
	public String deletePollById(@PathVariable int id) {
		try{
		return proxy.deletePollById(id);
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}
	
	@GetMapping("/{id}/getOption")
	public Options getOption(int id) {
		try {
		return proxy.getOption(id);
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}
	
	@PutMapping("/updateOption")
	public Options updateOption(@RequestBody Options option) {
		try {
		return proxy.updateOption(option);
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}
	
	@GetMapping("/{id}/delteOption")
	public String daleteOptions(int id) {
		try {
		return proxy.daleteOption(id);
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}
	
	@PostMapping("/{questionId}/{optionId}/calculatingVotesForEachOption")
	public String calculatingVotesForEachOption(@PathVariable("questionId") int questionId,
							@PathVariable("optionId") int optionId) {
		try {
		return proxy.calculatingVotesForEachOption(questionId, optionId);
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}
	
	
}
