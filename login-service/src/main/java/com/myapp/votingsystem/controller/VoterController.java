package com.myapp.votingsystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myapp.votingsystem.exception.NotFoundException;
import com.myapp.votingsystem.model.User;

@RestController
@RequestMapping("/voter")
public class VoterController {

	@Autowired
	public com.myapp.votingsystem.service.UserService userService;
	
	@Autowired
	public com.myapp.votingsystem.proxy.Proxy proxy;
	
	
	
	@PostMapping("/{questionId}/{optionId}/{voterId}/{comment}/polling")
	public String conductingPolling(@PathVariable("questionId") int questionId, @PathVariable("optionId") int optionId,
			@PathVariable("voterId") int voterId, @PathVariable("comment") String comment) {
		try {
		if(userService.isVoterEnteredProperVoterId(voterId)) {
		return proxy.conductingPolling(questionId, optionId, voterId, comment);
		}
		else {
			return "Enter Proper VoterId";
		}
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}
	@GetMapping("/{id}/getUser")
	public User finById(int id)
	{
		try {
		return userService.getUserById(id);
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}

	@DeleteMapping("/{id}/deleteUser")
	public String deleteUser(int id) {
		try {
		return userService.deleteUser(id);
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}
	
	@GetMapping("/getUsers")
	public List<User> getUsers() {
		return userService.getUsers();
	}

}
