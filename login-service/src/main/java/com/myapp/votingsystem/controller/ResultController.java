package com.myapp.votingsystem.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myapp.votingsystem.exception.NotFoundException;

@RestController
@RequestMapping("/result")
public class ResultController {

	@Autowired
	public com.myapp.votingsystem.proxy.Proxy proxy;
	
	@GetMapping("/{questionId}/finalResult")
	public String finalResult(@PathVariable("questionId")int questionId) {
		try {
		return proxy.finalResult(questionId);
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}
	
	@GetMapping("/{questionId}/{optionId}/getComments")
	public String[] fetchComments(@PathVariable("questionId")int questionId
							,@PathVariable("optionId")int optionId){
		try {
		return proxy.fetchComments(questionId, optionId);
		}catch(Exception e) {
			throw new NotFoundException();
		}
	}
}
