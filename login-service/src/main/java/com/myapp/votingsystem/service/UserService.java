package com.myapp.votingsystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.myapp.votingsystem.config.JwtTokenProvider;
import com.myapp.votingsystem.exception.CustomException;
import com.myapp.votingsystem.exception.UserAlreadyExistsException;
import com.myapp.votingsystem.model.User;
import com.myapp.votingsystem.repo.UserRepositry;

@Service
public class UserService {
	


	@Autowired
	public UserRepositry userRepositry;

	@Autowired
	protected AuthenticationManager authenticationManager;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	
	public UserService(UserRepositry userRepository) {
		this.userRepositry = userRepository;
	}

	// By using this user will be saved
	public User saveUser(User user) {
		if(userRepositry.findByEmailId(user.getEmailId())!=null) {
			throw new UserAlreadyExistsException("There is an account with that email address: " + user.getEmailId());
		}
		else {
			String password=user.getPassword();
		user.setPassword(passwordEncoder.encode(password));
		userRepositry.save(user);
		user.setPassword(password);
		 return user;
		}
	}

	// this is used to a user by using user id
	public User getUserById(int userId) {
		return userRepositry.getUserById(userId);
	}


	public String signin(String userName, String password) {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, password));
			return jwtTokenProvider.createToken(userName);
		} catch (AuthenticationException e) {
			throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
		}

	}

	public boolean isVoterEnteredProperVoterId(int voterId) {
		
		User user=userRepositry.getUserById(voterId);
		return user.getUserName().equals(jwtTokenProvider.getUserCheckName());
	}
//	public String signup(User user) {
//		if (!userRepositry.existsByUserName(user.getUserName())) {
//			String password=passwordEncoder.encode(user.getPassword());
//			user.setPassword(password);
//			userRepositry.save(user);
//			return jwtTokenProvider.createToken(user.getUserName());
//		} else {
//			throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
//		}
//	}

	public Boolean isUserExist(int id) {
		List<User> users= userRepositry.findAll();
		User user=null;
		for(User us:users) {
			if(us.getId()==id)
				user=us;
		}
		if (user != null)
			return true;
		else
		return false;
	}

	public User findByUserName(String userName) {
		
		return userRepositry.findByUserName(userName);
	}

	public String deleteUser(int id) {
		User user=userRepositry.getUserById(id);
		if(user!=null) {
		 userRepositry.deleteById(id);
		return "Deleted";
		}else
			return "check your entry";
	}

	public List<User> getUsers() {
		
		return userRepositry.findAll();
	}
	
}
