package com.myapp.votingsystem.proxy;

import java.util.List;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.myapp.votingsystem.model.Options;
import com.myapp.votingsystem.model.Question;

@FeignClient(name="admin-service",url="localhost:8001")
public interface Proxy {
	
	@PostMapping("/createPoll")
	public String creatingPoll(@Valid @RequestBody Question question);

	@GetMapping("/{pollId}/getPoll")
	public Question getPollById(@PathVariable("pollId") int id);
	
	@GetMapping("/getExistingPolls")
	public List<Question> getExistingPolls();
	
	@DeleteMapping("/{id}/deletePoll")
	public String deletePollById(@PathVariable int id);
	
	@GetMapping("/{questionId}/finalResult")
	public String finalResult(@PathVariable("questionId")int questionId);
	
	
	@GetMapping("/{questionId}/{optionId}/getComments")
	public String[] fetchComments(@PathVariable("questionId")int questionId
							,@PathVariable("optionId")int optionId);
	
	@PostMapping("/{questionId}/{optionId}/calculatingVotesForEachOption")
	public String calculatingVotesForEachOption(@PathVariable("questionId") int questionId,
							@PathVariable("optionId") int optionId);
	
	@PostMapping("/{questionId}/{optionId}/{voterId}/{comment}/polling")
	public String conductingPolling(@PathVariable("questionId") int questionId, @PathVariable("optionId") int optionId,
			@PathVariable("voterId") int voterId, @PathVariable("comment") String comment);
	
	@PutMapping("/updateOption")
	public Options updateOption(@RequestBody Options option);
	
	@DeleteMapping("/{id}/delteOption")
	public String daleteOption(int id);
	
	@GetMapping("/{id}/getOption")
	public Options getOption(int id);
	
	
	
}
